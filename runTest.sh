#!/bin/bash
TOP=$(cd $(dirname $0) && pwd)
REMOTE_HOST=$1
PAYLOAD_SIZE_IN_MEGS=$2
KEYFILE=""
if [ -f "$TOP/test_id_rsa" ]; then
  KEYFILE="-i $TOP/test_id_rsa"
fi

rm -f $TOP/payload.tmp
dd if=/dev/urandom of=$TOP/payload.tmp bs=1M count=$PAYLOAD_SIZE_IN_MEGS 2>&1 >> /dev/null
PAYLOAD_MD5=$(md5sum $TOP/payload.tmp)
echo "Payload MD5: $PAYLOAD_MD5"
scp $KEYFILE $TOP/payload.tmp $REMOTE_HOST:~/payload.tmp
REMOTE_MD5=$(ssh $KEYFILE $REMOTE_HOST "md5sum payload.tmp")
echo "Remote MD5: $REMOTE_MD5"
rm -f $TOP/payload.tmp

